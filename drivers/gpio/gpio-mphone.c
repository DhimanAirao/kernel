#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h> 
#include <linux/delay.h>
#include <linux/gpio.h>
#include <linux/of.h>
#include <linux/of_gpio.h>
#include <linux/of_platform.h>
#include <linux/uaccess.h>
#include <linux/fb.h>
#include <linux/device.h>
#include <linux/miscdevice.h>
#include <linux/fs.h>
#include <linux/string.h>
#include <linux/delay.h>
#define TAGLOG "NNNTIMES_XIXUN_GPIO"


#define INVALID_GPIO -1
int array_high_gpio[]={152};

int ntimes_gpio_active_l = 0;
int ntimes_gpio_active_h = 1;

struct gpio_read_data {
	int index;	// 0 或 1
	int level;	//gpio 号
	int dir;	//gpio 方向
}data;

static int ntimes_gpio_open(struct inode * inode, struct file * filp)
{
	//printk("%s %s line %d\n", TAGLOG, __FUNCTION__, __LINE__);
	return 0;	
}
static ssize_t  ntimes_gpio_read(struct file *filp,char __user *buf,size_t count,loff_t *fops)
{
	int i;
	int err = -1;
	if(copy_from_user(&data, buf, sizeof(data))){
		return -EFAULT;
	}
	
	for(i = 0; i< sizeof(array_high_gpio)/sizeof(int);i++){
		if(data.level == array_high_gpio[i])
		{
			data.index = gpio_get_value(array_high_gpio[i]);
			goto find ;
		}
	}

	find:
	err = copy_to_user(buf, &data ,sizeof(data));
	if(err){
	//	printk("%s %s line %d\n", TAGLOG, __FUNCTION__, __LINE__);
	}
	return count;
}
static int ntimes_gpio_release(struct inode *inode, struct file *file)
{
	printk("%s %s line %d\n", TAGLOG, __FUNCTION__, __LINE__);
	return 0;
}

static ssize_t ntimes_gpio_write(struct file * filp, const char __user *buf, size_t count, loff_t *fops){
	int ret;
	ret = copy_from_user(&data, buf, sizeof(data));
	if (ret) {
        return -EFAULT;
	}
	//printk("m555 copy from user data.index = %d , data.level = %d, data.dir = %d\n",data.index,data.level,data.dir);	//index 0,1   level gpionum  , dir 0(in) 1(out)
	if(data.dir == 0)
		gpio_direction_input(data.level);
	else if(data.dir == 1)
		gpio_direction_output(data.level,data.index);
	else
		return -1;
	return 0;
}

static const struct file_operations ntimes_gpio_fops = {
	.owner          = THIS_MODULE,
	.open           = ntimes_gpio_open,
	.read			= ntimes_gpio_read,
	.release        = ntimes_gpio_release,
	.write			= ntimes_gpio_write,
	//.unlocked_ioctl = ntimes_gpio_ioctl,
	//.compat_ioctl	= ntimes_gpio_ioctl,
};

static struct miscdevice ntimes_gpio_dev = {
	.minor = MISC_DYNAMIC_MINOR,
		.name = "ntimes_gpio",
		.fops = &ntimes_gpio_fops,
};

static int ntimes_gpio_probe(struct platform_device *pdev)
{
	int ret= -1;
	int i=0;
	printk("%s %s line %d\n", TAGLOG, __FUNCTION__, __LINE__);

	/* register test_gpio dev file */
	ret = misc_register(&ntimes_gpio_dev);
	if(ret<0)
	{
		printk("%s %s line %d\n", TAGLOG, __FUNCTION__, __LINE__);
		return ret;
	}
	
	for(i=0;i<sizeof(array_high_gpio)/sizeof(int);i++){
		if(!gpio_is_valid(array_high_gpio[i]))
			return -EFAULT;
		if(gpio_request(array_high_gpio[i],"ntimes_gpio"))
			return -EIO;
		gpio_direction_output(array_high_gpio[i],ntimes_gpio_active_h);//max<3.4
	}

	return 0;

}

static int ntimes_gpio_remove(struct platform_device *pdev)
{
	return 0;
}

	static const struct of_device_id of_ntimes_gpio_match[] = {
		{.compatible = "ntimes,mphone_gpio"},
			{/* sentinel */}
	};

static struct platform_driver ntimes_gpio_driver = {
	.probe = ntimes_gpio_probe,/*(struct platform_device * pdev)*/
	.remove = ntimes_gpio_remove,
	.driver = {
				.name = "ntimes_gpio",
				.owner = THIS_MODULE,
				.of_match_table = of_ntimes_gpio_match,
				},
};

static int ntimes_gpio_init(void){
	platform_driver_register(&ntimes_gpio_driver);
	printk("%s %s line %d\n", TAGLOG, __FUNCTION__, __LINE__);
	return 0;
}

static void ntimes_gpio_exit(void){
	platform_driver_unregister(&ntimes_gpio_driver);
	printk("%s %s line %d\n", TAGLOG, __FUNCTION__, __LINE__);
}

module_init(ntimes_gpio_init);
module_exit(ntimes_gpio_exit);

MODULE_AUTHOR("ntimes-m5");
MODULE_DESCRIPTION("nimes gpio driver");
MODULE_LICENSE("GPL");

